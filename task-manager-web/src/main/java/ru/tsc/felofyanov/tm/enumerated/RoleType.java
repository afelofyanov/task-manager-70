package ru.tsc.felofyanov.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public enum RoleType {
    ADMIN("Administration"),
    USUAL("Usual user");

    @Getter
    @NotNull
    private final String displayName;

    RoleType(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static RoleType toRole(@Nullable final String value) {
        if (value == null || value.isEmpty()) return USUAL;
        for (final RoleType role : values())
            if (role.name().equals(value)) return role;
        return USUAL;
    }
}
