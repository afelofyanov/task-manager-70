package ru.tsc.felofyanov.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.felofyanov.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findFirstByLogin(@Nullable String login);

    @Nullable
    User findFirstByEmail(@Nullable String email);

    @Nullable
    @Transactional
    User deleteByLogin(@Nullable String login);
}
