package ru.tsc.felofyanov.tm.util;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public interface GenericUtil {

    @NotNull
    @SneakyThrows
    static Class<?> getClassFirst(Class<?> clazz) {
        return Class.forName(getClassName(clazz, 0));
    }

    @NotNull
    @SneakyThrows
    static Class<?> getClassSecond(Class<?> clazz) {
        return Class.forName(getClassName(clazz, 1));
    }

    @NotNull
    static String getClassName(Class<?> clazz, int index) {
        @NotNull final Type type = clazz.getGenericSuperclass();
        @NotNull final ParameterizedType parameterizedType = (ParameterizedType) type;
        return parameterizedType.getActualTypeArguments()[index].getTypeName();
    }
}
