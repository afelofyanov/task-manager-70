package ru.tsc.felofyanov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.tsc.felofyanov.tm.api.service.IPropertyService;

import java.util.Properties;

@Getter
@Service
public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private final Properties properties = new Properties();

    @NotNull
    @Value("#{environment['password.iteration'] ?: '13425'}")
    private Integer passwordIteration;

    @NotNull
    @Value("#{environment['password.secret'] ?: '5566677234'}")
    private String passwordSecret;

    @NotNull
    @Value("#{environment['server.port'] ?: '8080'}")
    private Integer serverPort;

    @NotNull
    @Value("#{environment['session.key']}")
    private String sessionKey;

    @NotNull
    @Value("#{environment['session.timeout']}")
    private Integer sessionTimeout;

    @NotNull
    @Value("#{environment['database.username']}")
    private String databaseUserName;

    @NotNull
    @Value("#{environment['database.password']}")
    private String databaseUserPassword;

    @NotNull
    @Value("#{environment['database.url'] ?: 'jdbc:postgresql://localhost:5432/taskmanager'}")
    private String databaseUrl;

    @NotNull
    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @NotNull
    @Value("#{environment['database.sql_dialect']}")
    private String databaseDialect;

    @NotNull
    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @NotNull
    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @NotNull
    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @NotNull
    @Value("#{environment['database.cache.use_second_level_cache']}")
    private String databaseUseSecondL2Cache;

    @NotNull
    @Value("#{environment['database.cache.use_query_cache']}")
    private String databaseUseQueryCache;

    @NotNull
    @Value("#{environment['database.cache.use_minimal_puts']}")
    private String databaseUseMinimalPuts;

    @NotNull
    @Value("#{environment['database.cache.region_prefix']}")
    private String databaseRegionPrefix;

    @NotNull
    @Value("#{environment['database.cache.provider_configuration_file_resource_path']}")
    private String databaseProviderConfigurationFileResourcePath;

    @NotNull
    @Value("#{environment['database.cache.region.factory_class']}")
    private String databaseRegionFactoryClass;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }
}
